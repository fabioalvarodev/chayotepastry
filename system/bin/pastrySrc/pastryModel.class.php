<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace system\bin\pastrySrc;

/**
 * Model que verifica o status da tabela informada
 * @author  Matheus Scarpato Fidelis
 * @email   matheus.fidelis@industriafox.com
 * @date    16/06/2016
 */
class pastryModel extends \model {

  public $db;

  public function getInformationTable($table) {

    global $CFG;

    $this->db = new \PDO("mysql:host={$CFG->database->application->host};dbname={$CFG->database->application->schema};port={$CFG->database->application->port}", $CFG->database->application->user, $CFG->database->application->password, array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $CFG->database->application->charset));
    $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    $query = "DESCRIBE {$table}";
    $q = $this->db->prepare($query);
    $q->execute();
    $q->setFetchMode(\PDO::FETCH_ASSOC);
    $data = $q->fetchAll();

    //Verifica os tipos dos campos para a criação dos tipos dos inputs
    foreach ($data as $key => $reg) {
      if ($this->startsWith($reg['Type'], 'int')) {
        $data[$key]['input'] = 'numeric';
      } elseif ($this->startsWith($reg['Type'], 'varchar')) {
        $data[$key]['input'] = 'text';
      } elseif ($this->startsWith($reg['Type'], 'text')) {
        $data[$key]['input'] = 'textarea';
      } elseif ($this->startsWith($reg['Type'], 'datetime')) {
        $data[$key]['input'] = 'date';
      } elseif ($this->startsWith($reg['Type'], 'float') OR $this->startsWith($reg['Type'], 'decimal')) {
        $data[$key]['input'] = 'float';
      }
    }

    return $data;
  }

  public function getCreateStatement($table) {

    global $CFG;

    $this->db = new \PDO("mysql:host={$CFG->database->application->host};dbname={$CFG->database->application->schema};port={$CFG->database->application->port}", $CFG->database->application->user, $CFG->database->application->password, array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $CFG->database->application->charset));
    $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    $query = "SHOW CREATE TABLE {$table}";
    $q = $this->db->prepare($query);
    $q->execute();
    $q->setFetchMode(\PDO::FETCH_ASSOC);
    $data = $q->fetchAll();
    return $data[0]['Create Table'];
  }

  public function startsWith($haystack, $needle) {
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
  }

  public function endsWith($haystack, $needle) {
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
  }

}
