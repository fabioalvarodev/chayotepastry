<?php

namespace system\bin;

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../load.inc";
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../controllers/pastryController.php";

$commands = getopt("c:m:h:");

class shelloutput {

  private $foreground_colors = array();
  private $background_colors = array();

  public function __construct() {
    $this->foreground_colors['black'] = '0;30';
    $this->foreground_colors['dark_gray'] = '1;30';
    $this->foreground_colors['blue'] = '0;34';
    $this->foreground_colors['light_blue'] = '1;34';
    $this->foreground_colors['green'] = '0;32';
    $this->foreground_colors['light_green'] = '1;32';
    $this->foreground_colors['cyan'] = '0;36';
    $this->foreground_colors['light_cyan'] = '1;36';
    $this->foreground_colors['red'] = '0;31';
    $this->foreground_colors['light_red'] = '1;31';
    $this->foreground_colors['purple'] = '0;35';
    $this->foreground_colors['light_purple'] = '1;35';
    $this->foreground_colors['brown'] = '0;33';
    $this->foreground_colors['yellow'] = '1;33';
    $this->foreground_colors['light_gray'] = '0;37';
    $this->foreground_colors['white'] = '1;37';

    $this->background_colors['black'] = '40';
    $this->background_colors['red'] = '41';
    $this->background_colors['green'] = '42';
    $this->background_colors['yellow'] = '43';
    $this->background_colors['blue'] = '44';
    $this->background_colors['magenta'] = '45';
    $this->background_colors['cyan'] = '46';
    $this->background_colors['light_gray'] = '47';
  }

  // Returns colored string
  public function getColoredString($string, $foreground_color = null, $background_color = null) {
    $colored_string = "";

    if (isset($this->foreground_colors[$foreground_color])) {
      $colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
    }

    if (isset($this->background_colors[$background_color])) {
      $colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
    }

    $colored_string .= $string . "\033[0m";

    echo $colored_string;
  }

}

$shell = new shelloutput();

(new \pastry())->build($commands);

$shell->getColoredString("==    PASTRY - TELINHA GENERATOR v1.0      ==\n", "light_green");
$shell->getColoredString("==  AUTHOR: Matheus Fidelis @fidelissauro  ==\n", "light_green");
$shell->getColoredString("== GERADOR DE CRUDS SIMPLES DO GESTOR FOX  ==\n\n", "light_green");
$shell->getColoredString("Ainda na versão de testes. Por favor, me mande um feedback: matheus.fidelis@industriafox.com\n\n", "light_green");

$shell->getColoredString("== CONTROLLER == CRIADO EM: controllers/{$commands['c']}Controller.php\n\n", "light_green");
$shell->getColoredString("==   MODEL    == CRIADO EM: models/{$commands['m']}Model.php\n\n", "light_green");
$shell->getColoredString("==  CLASSES   == CRIADAS EM: controllers/{$commands['c']}/Cadastrar.class.php\n", "light_green");
$shell->getColoredString("                             controllers/{$commands['c']}/Excluir.class.php\n", "light_green");
$shell->getColoredString("                             controllers/{$commands['c']}/IndexAction.class.php\n", "light_green");
$shell->getColoredString("==   VIEWS    == CRIADAS EM: views/{$commands['c']}/cadastrar.tpl\n", "light_green");
$shell->getColoredString("                             views/{$commands['c']}/index.tpl\n\n", "light_green");
$shell->getColoredString("== JAVASCRIPT == CRIADO EM: public/files/default/js/{$commands['c']}/excluir.js\n\n", "light_green");

$shell->getColoredString("FUNCIONALIDADES CRIADAS NA VERSÃO: \n", "light_green");
$shell->getColoredString("[✓] INDEX + LISTA\n", "light_green");
$shell->getColoredString("[✓] TELAS RESPONSIVAS\n", "light_green");
$shell->getColoredString("[✓] CADASTRAR\n", "light_green");
$shell->getColoredString("[✓] ALTERAR\n", "light_green");
$shell->getColoredString("[✓] BUSCAR\n", "light_green");
$shell->getColoredString("[✓] EXCLUIR\n\n", "light_green");

$shell->getColoredString("BOA SORTE! :D\n\n", "light_green");



