<?php

namespace controllers\{$controllername};

use {$modelname}Model;
use system\Action;

/**
 * Classe responsável pelos controlles da Index
 * @author  Chayote Pastry v.1.0
 * @email   matheus.fidelis@industriafox.com
 * @date    {date("d-m-Y")|date_format: 'd/m/Y'}
 */
class IndexAction extends Action {

  public function render() {
    $this->renderRegisters();
    $template = $this->controller->smarty->fetch('{$controllername}/index.tpl');
    $this->renderTemplate($template);
  }

  private function renderRegisters() {
    if ($this->controller->getParam('pagina') && isset($_SESSION['{$controllername}']['query'])) {
      $where = $_SESSION['{$controllername}']['query'];
      $this->controller->smarty->assign('busca', $_SESSION['{$controllername}']['filter']);
    } else {
      $where = $this->render_filters($this->controller->getParamPost());
    }
    $this->controller->smarty->assign('registers', $this->renderLista($where));
  }

  private function render_filters() {
    $itensbusca = array();
    $where = "stat = 1";
    

    {foreach $campos as $campo}
    ${$campo['Field']} = filter_input(INPUT_POST, "{$campo['Field']}");
        if (${$campo['Field']}) {
            $where .= " AND {$campo['Field']} =  '${$campo['Field']}'"; 
            $itensbusca['{$campo['Field']}'] = ${$campo['Field']}; 
        }
        
   
    {/foreach}

    $_SESSION['{$controllername}']['query'] = $where;
    $_SESSION['{$controllername}']['filter'] = $itensbusca;
    $this->controller->smarty->assign('busca', $itensbusca);
    return $where;
  }

  private function renderLista($where) {
    $model = new {$modelname}Model();
    return $model->findAll($where, NULL, TRUE);
  }

}
