<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controllers\{$controllername};

use {$modelname}Model;
use system\Action;

/**
 * Classe responsável por excluir um registro do crud
 * @author  Chayote Pastry v.1.0
 * @email   matheus.fidelis@industriafox.com
 * @date    {date("d-m-Y")|date_format: 'd/m/Y'}
 */
class Excluir extends Action {

  public function delete($id) {
    $model = new {$modelname}Model();
    $model->delRow($id);
    $this->pageReturn();
  }

  public function setMessage() {
    $_SESSION['retorno']['tipo'] = 'success';
    $_SESSION['retorno']['msg'] = $this->controller->excluido_sucesso;
  }

  public function pageReturn() {
    $this->setMessage();
    header("Location:/{$controllername}");
  }

}
