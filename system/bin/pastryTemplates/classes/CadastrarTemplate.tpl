<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controllers\{$controllername};
use {$modelname}Model;
use system\Action;


/**
 * Classe responsável pela aba de Dados Básicos
 * @author  Chayote Pastry v.1.0
 * @email   matheus.fidelis@industriafox.com
 * @date    {date("d-m-Y")|date_format: 'd/m/Y'}
 */
class Cadastrar extends Action {
  
   public function render($id = NULL) {
    $this->renderRegisters($id);
    $template = $this->controller->smarty->fetch('{$controllername}/cadastrar.tpl');
    $this->renderTemplate($template);
  }

  private function renderRegisters($id = null) {
    $this->controller->smarty->assign('data', $this->getData($id));
  }

  private function getData($id = null) {
    if (!empty($id)) {
      $model = new {$modelname}Model();
      $data = $model->findSimpleRow($id);
    } else {
      $data = null;
    }
    return $data;
  }

  public function isSave() {
    if ((bool) $this->controller->getParam('save')) {
      return True;
    } else {
      return False;
    }
  }

  public function save($data) {
    $model = new {$modelname}Model();
    if (!empty($data['{$id_model}'])) {
      $model->updRow($data);
      $id = $data['{$id_model}'];
    } else {
      $id = $model->setRow($data);
    }
    $this->setMessage();
    $this->redirect($id);
  }

  public function setMessage() {
    $_SESSION['retorno']['tipo'] = 'success';
    $_SESSION['retorno']['msg'] = $this->controller->gravado_sucesso;
  }

  private function redirect($id) {
    header("Location:/{$controllername}/cadastrar/{$id_model}/" . $id);
  }


}
