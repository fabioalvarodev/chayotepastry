<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author  Chayote Pastry
 * @email   matheus.fidelis@industriafox.com
 * @date    16/06/2016
 */
class {$tabName}Model extends model {

  var $tabPadrao = '{$tabName}';
  var $campoChave = '{$campoChave}';
  protected $version = {$version};
  
  public function getUpdateQueryes($version) {
    if ($version < {$version}) {
      $queryes[] = "{$createArray}";
    } 
    return $queryes;
    }

  /**
   * 
   * @param array $data
   * @return int {$campoChave}
   */
  public function setRow(Array $data) {
    return $this->insert($this->tabPadrao, $data);
  }

  /**
   * 
   * @param array $data
   * @return array
   */
  public function updRow(Array $data) {
    return $this->update($this->tabPadrao, $data, $this->campoChave . "=" . $data[$this->campoChave]);
  }

  /**
   * 
   * @param int $id {$campoChave}
   * @return int {$campoChave}
   */
  public function delRow($id) {
    return $this->update($this->tabPadrao, array("stat" => 0), $this->campoChave . "=" . $id);
  }

  /**
   * 
   * @param type $where
   * @param type $orderby
   * @param type $paginacao
   * @return type array
   */
  public function findAll($where = "stat = 1", $orderby = NULL, $paginacao = FALSE) {
    $fields = array("*");
    $tables = "{literal}$this->tabPadrao{/literal}";
    return $this->read($tables, $fields, $where, NULL, NULL, NULL, $orderby, NULL, NULL, $paginacao);
  }

  /**
   * Busca todos os dados referntes a id  informado
   * @param type $id int {$campoChave}
   * @return type
   */
  public function findSimpleRow($id) {
    $where = "{literal}$this->campoChave {/literal}= " . $id . " AND stat = 1";
    $result = $this->findAll($where);

    if (!empty($result)) {
      return $result[0];
    } else {
      return NULL;
    }
  }

}
