<script src="/files/default/js/{$controllername}/excluir.js" type="text/javascript"></script>
<form id="frm-cadastrar" name="frm-cadastrar" method="post" action="/{$controllername}/cadastrar/save/1">          
    <div class="row row-button-top"> 
        <div class="col-xs-12 col-md-2 col-lg-2 button-m-top pull-right">
            <a href="/{$controllername}" class="btn btn-default btn-retornar form-control">RETORNAR</a>
        </div>
        <div class="col-xs-12 col-md-2 col-lg-2 button-m-top pull-right">
            <input type="submit" id="salvar" class="btn btn-default btn-salvar form-control"value="SALVAR"/>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <li><a href="/{$controllername}">BUSCAR</a></li>
        <li class="active"><a data-toggle="tab">DADOS BASICOS</a></li>      
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="panel panel-default">    
            <div class="panel-body">     
                <div class="col-xs-12 col-md-12 col-lg-12">  
                    <div class="row">

                        {foreach $campos as $campo}
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="{$campo.Field}">{$campo.Field}</label>
                                    <input type="text" class="form-control {if $campo.Null == "NO" AND $campo.Key != "PRI"} obg {/if}" name="{$campo.Field}" id="{$campo.Field}" {if $campo.Key == "PRI"} readonly="True" {/if} value="{literal}{$data.{/literal}{$campo.Field}{literal}|default:''}{/literal}"/>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
</form>   
