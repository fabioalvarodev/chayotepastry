
<script src="/files/default/js/{$controllername}/excluir.js" type="text/javascript"></script>

<div class="row row-button-top">
    <div class="col-xs-12 col-md-2 col-lg-2 button-m-top pull-right">
        <a class="btn btn-default btn-inserir form-control" href="/{$controllername}/cadastrar">INSERIR</a>
    </div>
</div>

<ul class="nav nav-tabs">
    <li id="buscar_aba" class="active"><a href="/{$controllername}" data-toggle="tab">BUSCAR</a></li>
    <li class="disabled"><a>DADOS BASICOS</a></li>  
</ul>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane fade active in" id="buscar_aba">
        <div class="panel panel-default">    
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">
                        <form method="post" name="frm-busca" action="/{$controllername}" >
                            <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        {foreach $campos as $campo}
                                            <th>{$campo['Field']} </th>
                                            {/foreach}
                                        <th class="text-center">ACAO</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <div class="row">
                                    <tr class="linha_busca">

                                        {foreach $campos as $campo}
                                            <td>
                                                <input type="text" data-label="" class="form-control {if $campo['Type'] == "INT"} numeric standard-mask-integer standard-form-max-11c{/if}" id="{$campo['Field']}" name="{$campo['Field']}" maxlength="11" value="{literal}{$busca{/literal}.{$campo['Field']}{literal}|default:''}{/literal}"/>
                                            </td>
                                        {/foreach}

                                        <td>
                                            <div class="row text-center">
                                                <div class="col-md-12">
                                                    <div class="btn-group btn-group-sm" role="group">
                                                        <button type="submit" id="buscar" class="btn btn-default btn-buscar">BUSCAR</button>
                                                        <div class="btn-group btn-group-sm" role="group">
                                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="/{$controllername}">LIMPAR</a></li>
                                                                <li><a href="/{$controllername}/listar" target="_blank">LISTAR</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    {literal}
                                        {foreach $registers as $register}
                                    {/literal}
                                    <tr>
                                        {foreach $campos as $campo}
                                            <td>
                                                {literal}{$register{/literal}.{$campo['Field']}{literal}|default:''}{/literal}
                                            </td>
                                        {/foreach}

                                        <td class='text-center'>
                                            <a class="btn btn-default btn-alterar btn-mini" title="ALTERAR" href="/{$controllername}/cadastrar/{$id_model}/{literal}{$register.{/literal}{$id_model}{literal}}{/literal}">A</a>
                                            <a class="btn btn-default btn-excluir btn-mini" title="EXCLUIR" onclick="confirma_excluir_registro({literal}{$register.{/literal}{$id_model}{literal}}{/literal});">E</a> 
                                        </td>
                                    </tr>
                                    {literal}
                                        {foreachelse}
                                        <tr>
                                            <td colspan="100%">NENHUM REGISTRO ENCONTRADO</td>
                                        </tr>
                                        {/foreach}
                                    {/literal}
                                </div>
                                </tbody>
                            </table>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        {literal}{$paginacao|default:''}{/literal}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
