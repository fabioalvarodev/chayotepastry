<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use controllers\{$controllername}\IndexAction;
use controllers\{$controllername}\Cadastrar;
use controllers\{$controllername}\Excluir;
 
 
/**
 * @author  Chayote Pastry v.1.0
 * @email   matheus.fidelis@industriafox.com
 * @date    {date("d-m-Y")|date_format: 'd/m/Y'}
 */
 
class {$controllername} extends controller {
  /**
   * Função responsável pela renderização da Index
   */
  public function index_action() {
    $class = new IndexAction($this);
    $class->render();
  }

  /**
   * Função responsável pela renderização tela de Cadastrar
   */
  public function cadastrar() {
    $class = new Cadastrar($this);
    if ($class->isSave()) {
      $class->save($this->getParamPost());
    } else {
      $class->render($this->getParam('{$id_model}'));
    }
  }


  /**
   * Função responsável por excluir um registro
   */
  public function excluir() {
    $class = new Excluir($this);
    $class->delete($this->getParam('id'));
  }

  /**
   * Função responsável pelo listar (Não funcionando ainda :D )
   */    
  public function listar() {
    $class = new Listar($this);
    $class->render();
  }
}
