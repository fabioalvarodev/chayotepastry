<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use controllers\pastry\renderController;
use controllers\pastry\renderClasses;
use controllers\pastry\renderModel;
use controllers\pastry\renderViews;

/**
 * Controller do Pastry
 * @author  Matheus Scarpato Fidelis
 * @email   matheus.fidelis@industriafox.com
 * @date    16/06/2016
 */
class pastry extends controller {

  private $entity_name;
  private $model_name;

  public function getEntity_name() {
    return $this->entity_name;
  }

  public function getModel_name() {
    return $this->model_name;
  }

  public function setEntity_name($entity_name) {
    $this->entity_name = $entity_name;
  }

  public function setModel_name($model_name) {
    $this->model_name = $model_name;
  }

  public function index_action() {
    header("Location: /");
  }

  public function build($cmd) {
    $this->setEntity_name($cmd['c']);
    $this->setModel_name($cmd['m']);
    $this->createEntity();
  }

  private function createEntity() {
    $modelItens = $this->createModel();
    $this->createController($modelItens);
    $this->createClasses($modelItens);
    $this->createViews($modelItens);
  }

  private function createModel() {
    $controllerPastry = new renderModel($this);
    return $controllerPastry->create($this->model_name);
  }

  private function createController(array $infos) {
    $id_key = $infos['id_model']; unset($infos['id_model']);
    $controllerPastry = new renderController($this);
    $controllerPastry->create($this->entity_name, $this->model_name, $infos, $id_key);
  }
  
  private function createClasses(array $infos) {
    $id_key = $infos['id_model']; unset($infos['id_model']);
    $classesPastry = new renderClasses($this);
    $classesPastry->create($this->entity_name, $this->model_name, $infos, $id_key);
  }
  
  private function createViews(array $infos) {
    $id_key = $infos['id_model']; unset($infos['id_model']);
    $viewsPastry = new renderViews($this);
    $viewsPastry->create($this->entity_name, $this->model_name, $infos, $id_key);
    
  }

}
