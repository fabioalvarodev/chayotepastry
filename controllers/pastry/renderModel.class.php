<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controllers\pastry;

use system\Action;

//use system\bin\pastrySrc\pastryModel;

/**
 *
 * @author  Matheus Scarpato Fidelis
 * @email   matheus.fidelis@industriafox.com
 * @date    16/06/2016
 */
class renderModel extends Action {

  private $template;
  private $campoChave;
  private $tableData;
  private $tableName;
  private $callCampoChave = '{$this->campoChave}';
  private $callTabPadrao = '{$this->tabPadrao}';

  public function create($table_name) {
    $this->tableName = $table_name;

    $this->setTemplate();
    $model = new \system\bin\pastrySrc\pastryModel();
    $tableData = $this->findStat($model->getInformationTable($table_name));
    $createState = $model->getCreateStatement($table_name);
    $this->campoChave = $this->findCampoChave($tableData);
    
    $this->controller->smarty->assign('version', date("YmdHi"));
    $this->controller->smarty->assign('tabName', $this->tableName);
    $this->controller->smarty->assign('campoChave', $this->campoChave);
    $this->controller->smarty->assign('createArray', $createState);
    $this->controller->smarty->assign('callCampoChave', $this->callCampoChave);
    $this->controller->smarty->assign('callCampoChave', $this->callTabPadrao);
    $template = $this->controller->smarty->fetch($this->template);
    
    $this->createModel($template);
    
    $tableData['id_model'] = $this->campoChave; // Set ao campo chave da tabela
    unset($tableData['stat']); // Remove o campo stat do array
    return $tableData;
  }

  /**
   * Cria o arquivo físico do Model
   * @param type $template
   */
  public function createModel($template) {
    $filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../models/" . $this->tableName . "Model.php";
    $novoModel = fopen($filename, "w");
    fwrite($novoModel, $template);
    fclose($novoModel);
  }

  /**
   * Seta o template padrão do Model
   */
  private function setTemplate() {
    $tpl = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/modelTemplate.tpl";
    $this->template = $tpl;
  }

  /**
   * Retorna o campo chave da tabela 
   * @param type $colunas
   * @return type
   */
  private function findCampoChave($colunas) {
    foreach ($colunas as $coluna) {
      if ($coluna['Key'] == "PRI") {
        return $coluna['Field'];
      }
    }
  }

  /**
   * Retorna o campo chave da tabela 
   * @param type $colunas
   * @return type
   */
  private function findStat($colunas) {
    foreach ($colunas as $key => $coluna) {
      if ($coluna['Field'] == "stat") {
        unset($colunas[$key]);
        return $colunas;
      }
    }
  }

}
