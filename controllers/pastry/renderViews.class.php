<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controllers\pastry;

use system\Action;

/**
 *
 * @author  Matheus Scarpato Fidelis
 * @email   matheus.fidelis@industriafox.com
 * @date    16/06/2016
 */
class renderViews extends Action {

  private $indexTemplate;
  private $cadastrarTemplate;

  private function setTemplates() {
    // Template do Cadastrar
    $tplCadastrar = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/views/cadastrar.tpl";
    $this->cadastrarTemplate = $tplCadastrar;

    // Template da Index
    $tplIndex = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/views/index.tpl";
    $this->indexTemplate = $tplIndex;
  }

  /**
   * Metodo Core da Classe
   * @param type $controller_name nome da entidade
   * @param type $model_name nome do model
   * @param type $campos array com os nomes dos campos 
   */
  public function create($controller_name, $model_name, $campos, $key) {
    $this->setTemplates();
    $this->createFolder($controller_name);
    $this->createCadastrarTPL($controller_name, $model_name, $campos, $key);
    $this->createIndexTPL($controller_name, $model_name, $campos, $key);
    $this->createJSFolder($controller_name, $model_name, $campos, $key);
  }

  private function createIndexTPL($controller_name, $model_name, $campos, $key) {
    $this->controller->smarty->assign('controllername', $controller_name);
    $this->controller->smarty->assign('modelname', $model_name);
    $this->controller->smarty->assign('id_model', $key);
    $this->controller->smarty->assign('campos', $campos);

    $template = $this->controller->smarty->fetch($this->indexTemplate);
    $this->createView($template, $controller_name, 'index');
  }

  private function createCadastrarTPL($controller_name, $model_name, $campos, $key) {
    $this->controller->smarty->assign('controllername', $controller_name);
    $this->controller->smarty->assign('modelname', $model_name);
    $this->controller->smarty->assign('id_model', $key);
    $this->controller->smarty->assign('campos', $campos);

    $template = $this->controller->smarty->fetch($this->cadastrarTemplate);
    $this->createView($template, $controller_name, 'cadastrar');
  }

  private function createJSFolder($controller_name) {
    $directory = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../public/files/default/js/" . $controller_name . "/";
    mkdir($directory, 0777);
    
    $this->controller->smarty->assign('controllername', $controller_name);
    $js = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/views/ExcluirJSTemplate.tpl";
    $template = $this->controller->smarty->fetch($js);
    $filename = $directory . "excluir.js";
    
    $novoJS = fopen($filename, "w");
    fwrite($novoJS, $template);
    fclose($novoJS);
  }

  /**
   * Cria o template na pasta do views.
   * @param type $template
   * @param type $controller_name
   * @param type $view
   */
  private function createView($template, $controller_name, $view) {
    $filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../views/" . $controller_name . "/" . $view . ".tpl";
    $novoController = fopen($filename, "w");
    fwrite($novoController, $template);
    fclose($novoController);
  }

  /**
   * Cria a pasta da classe no diretório de views
   * @param type $controller_name
   * @return type
   */
  private function createFolder($controller_name) {
    try {
      $directory = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../views/" . $controller_name . "/";
      return mkdir($directory, 0777);
    } catch (Exception $e) {
      return true;
    }
  }

}
