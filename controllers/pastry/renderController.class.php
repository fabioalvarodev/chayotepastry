<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controllers\pastry;

use system\Action;

/**
 *
 * @author  Matheus Scarpato Fidelis
 * @email   matheus.fidelis@industriafox.com
 * @date    16/06/2016
 */
class renderController extends Action {

  private $template;

  public function create($controller_name, $model_name, $campos, $key) {
    $this->setTemplate();
    $this->controller->smarty->assign('controllername', $controller_name);
    $this->controller->smarty->assign('modelname', $model_name);
    $this->controller->smarty->assign('id_model', $key);
    $template = $this->controller->smarty->fetch($this->template);
    $this->createController($template, $controller_name);
  }

  private function setTemplate() {
    $tpl = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/controllerTemplate.tpl";
    $this->template = $tpl;
  }

  private function createController($template, $controller) {
    $filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../controllers/" . $controller . "Controller.php";
    $novoController = fopen($filename, "w");
    fwrite($novoController, $template);
    fclose($novoController);
  }

}
