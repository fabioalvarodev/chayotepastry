<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controllers\pastry;

use system\Action;

/**
 *
 * @author  Matheus Scarpato Fidelis
 * @email   matheus.fidelis@industriafox.com
 * @date    16/06/2016
 */
class renderClasses extends Action {

  private $templateCadastrar;
  private $templateIndex;
  private $templateExcluir;
  private $controller_name;
  private $model_name;
  private $fields;

  public function getTemplateCadastrar() {
    return $this->templateCadastrar;
  }

  public function getTemplateIndex() {
    return $this->templateIndex;
  }

  public function getController() {
    return $this->controller;
  }

  public function getModel() {
    return $this->model;
  }

  public function getFields() {
    return $this->fields;
  }

  public function setController($controller) {
    $this->controller = $controller;
  }

  public function setModel($model) {
    $this->model = $model;
  }

  public function setFields($fields) {
    $this->fields = $fields;
  }

  /**
   * Metodo Core da Classe
   * @param type $controller_name nome da entidade
   * @param type $model_name nome do model
   * @param type $campos array com os nomes dos campos 
   */
  public function create($controller_name, $model_name, $campos, $key) {
    $this->createFolder($controller_name);
    $this->renderCadastrar($controller_name, $model_name, $key);
    $this->renderIndex($controller_name, $model_name, $campos, $key);
    $this->renderExcluir($controller_name, $model_name, $campos, $key);
  }

  /**
   * Função responsável por criar o modelo físico da classe de cadastrar 
   * @param type $controller_name
   * @param type $model_name
   * @param type $campos
   */
  private function renderCadastrar($controller_name, $model_name, $key) {
    $this->setTemplateCadastro();
    $this->controller->smarty->assign('controllername', $controller_name);
    $this->controller->smarty->assign('modelname', $model_name);
    $this->controller->smarty->assign('id_model', $key);

    //Cria o template
    $template = $this->controller->smarty->fetch($this->templateCadastrar);
    $this->createClass($template, $controller_name, "Cadastrar");
  }

  /**
   * Cria o template da classe de cadastro
   */
  private function setTemplateCadastro() {
    $tpl = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/classes/CadastrarTemplate.tpl";
    $this->templateCadastrar = $tpl;
  }

  private function renderIndex($controller_name, $model_name, $campos, $key) {
    $this->setTemplateIndex();
    $this->controller->smarty->assign('controllername', $controller_name);
    $this->controller->smarty->assign('modelname', $model_name);
    $this->controller->smarty->assign('id_model', $key);
    $this->controller->smarty->assign('campos', $campos);

    $template = $this->controller->smarty->fetch($this->templateIndex);
    $this->createClass($template, $controller_name, 'IndexAction');
  }

  /**
   * Cria o template da classe do Index
   */
  private function setTemplateIndex() {
    $tpl = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/classes/IndexTemplate.tpl";
    $this->templateIndex = $tpl;
  }

  private function renderExcluir($controller_name, $model_name, $campos, $key) {
    $this->setTemplateExcluir();
    $this->controller->smarty->assign('controllername', $controller_name);
    $this->controller->smarty->assign('modelname', $model_name);
    $this->controller->smarty->assign('id_model', $key);
    $this->controller->smarty->assign('campos', $campos);

    $template = $this->controller->smarty->fetch($this->templateExcluir);
    $this->createClass($template, $controller_name, 'Excluir');
  }

  /**
   * Cria o template da classe do Index
   */
  private function setTemplateExcluir() {
    $tpl = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../system/bin/pastryTemplates/classes/ExcluirTemplate.tpl";
    $this->templateExcluir = $tpl;
  }

  /**
   * Cria a classe na pasta do controller.
   * @param type $template
   * @param type $controller_name
   * @param type $class
   */
  private function createClass($template, $controller_name, $class) {
    $filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../controllers/" . $controller_name . "/" . $class . ".class.php";
    $novoController = fopen($filename, "w");
    fwrite($novoController, $template);
    fclose($novoController);
  }

  /**
   * Cria a pasta da classe no diretório de controllers
   * @param type $controller_name
   * @return type
   */
  private function createFolder($controller_name) {
    try {
      $directory = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../controllers/" . $controller_name . "/";
      return mkdir($directory, 0777);
    } catch (Exception $e) {
      return true;
    }
  }

}
